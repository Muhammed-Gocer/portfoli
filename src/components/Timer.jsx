import { useEffect, useState } from 'react'
import styles from './Timer.module.css'


export default function Timer(){
    const [nombre,setNombre] = useState(10)
    
    useEffect(()=>{

        //Exécuter au montage
        console.log('Monter');
        let intervalId =setInterval(()=>{
            setNombre((vieuxnombre) =>{
                if(vieuxnombre >0){
                    return vieuxnombre -1
                }
                else
                {
                    clearInterval(intervalId);
                    return 0;
                }
            });
        }, 1000);
           
        
        //Exécuter au démontage
        return () =>{
            clearInterval(intervalId);
        
        }
    }, []);

    return <div className={styles.timer}>
        {nombre}
    </div>

}