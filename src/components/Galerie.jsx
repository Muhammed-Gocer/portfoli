import styles from './Galerie.module.css'

export default function Galerie(props){
    return <div className={styles.galerie}>
        {props.images.map((element, index) =>{
            return <img src={element.src} alt={element.alt} key={index}/>
        })
        }
    </div>
};
  
