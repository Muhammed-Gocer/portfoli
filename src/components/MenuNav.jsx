import styles from './MenuNax.module.css'
export default function MenuNav(props){
    return (
     <nav >
        <ul className={styles.list}>
            <li>
                <button onClick={props.changePage('acceuil')}>Acceuil</button>
            </li>
            <li>
                <button onClick={props.changePage('projet1')}>Projet 1</button>
                </li>
            <li>
                <button>Contacts</button>
            </li>
        </ul>
     </nav>
     
    );
}