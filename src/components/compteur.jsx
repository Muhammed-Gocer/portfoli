import { useState } from "react"
import styles from "./Compteur.module.css"

export default function Compteur(){
   const[nombre, setCompteur]= useState(0);

   const incrementeNombre = () =>{
    setCompteur(nombre + 1);
   }

    return <div className={styles.compteur}>
        <span className={styles.nombre}>{nombre}</span>
        <input type="button"  value="Compter" className={styles.button} onClick={incrementeNombre}/>
    </div>
}