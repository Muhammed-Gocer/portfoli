import styles from './Header.module.css'
import MenuNav from './MenuNav';
export default function Header(props){
    return( <header className={styles.header}>
        <MenuNav changePage={props.changePage}/>
   
    </header>
    );
}