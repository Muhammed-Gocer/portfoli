import ContentToggler from '../components/ContentToggler';
import Galerie from '../components/Galerie';
import Timer from '../components/Timer';

import tabpokemon from '../resources/gallerie-pokemon.json'

export default function Projet1() {
    

    return <>
     <ContentToggler title="Test content toggler">
        <p>Lorem lorem lorem lorem lorem lorem lorem</p>
     </ContentToggler>

     <Timer />

     <Galerie  images={tabpokemon}>
       
     </Galerie>
    </>
    
}