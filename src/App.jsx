import Header from './components/Header';
import Projet1 from './pages/Projet1';
import Acceuil from './pages/Acceuil';
import { useState } from 'react';
import './App.css';


export default function App() {
  const [pageCourante, setPageCourante] = useState('acceuil');
 const changePage = (page) =>{
   return() => {
     setPageCourante(page);
   }
 }

  return <>

     <Header changePage={changePage}/>

     

     {pageCourante === 'acceuil' &&
     <Acceuil/>

     }

    {pageCourante === 'projet1' &&
     <Projet1/>

     }  
    
    
  </>
}

